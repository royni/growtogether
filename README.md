# README #

Cliffnotes learn ios from iOS Apprentice book by raywenderlich.

### Bullseye ###

* [Basic SwiftUI](https://bitbucket.org/royni/growtogether/src/basic-swift-ui)
* [Function](https://bitbucket.org/royni/growtogether/src/function-swift-ui)
* [Styling](https://bitbucket.org/royni/growtogether/src/styling-swift-ui)
* [Navigation](https://bitbucket.org/royni/growtogether/src/navigation-view)
* [App icon](https://bitbucket.org/royni/growtogether/src/app-icon)


### Checklists ###

* [List](https://bitbucket.org/royni/growtogether/src/checklist-list)
* [Struct](https://bitbucket.org/royni/growtogether/src/cehcklist-check-able)
* [MVVM](https://bitbucket.org/royni/growtogether/src/mvvm/)
* [Add Item](https://bitbucket.org/royni/growtogether/src/checklist-add-item/)
* [Edit](https://bitbucket.org/royni/growtogether/src/cheklist-edit/)